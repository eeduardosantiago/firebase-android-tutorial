package com.example.cess.firebasetuturial;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import de.hdodenhof.circleimageview.CircleImageView;

public class SettingsActivity extends AppCompatActivity {

    private static final int PICK_IMAGE = 1;
    DatabaseReference mDatabaseUser;
    FirebaseUser currentUser;

    TextView displayName;
    TextView settingsStatus;

    Button trocarStatusBtn;
    Button trocarImagemBtn;

    CircleImageView mImageView;

    //Storage Firebase
    StorageReference mStorage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        displayName = (TextView)findViewById(R.id.settingsPage_displayName);
        settingsStatus = (TextView)findViewById(R.id.settingsPage_status);

        trocarStatusBtn = (Button)findViewById(R.id.settingsPage_changeStatus);
        trocarImagemBtn = (Button)findViewById(R.id.settingsPage_changeImage);

        mImageView = (CircleImageView)findViewById(R.id.settingsPage_circleImageView);

        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        mDatabaseUser = FirebaseDatabase.getInstance().getReference().child("users").child(currentUser.getUid());

        mStorage = FirebaseStorage.getInstance().getReference();

        mDatabaseUser.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String name = dataSnapshot.child("nome").getValue().toString();
                String status = dataSnapshot.child("status").getValue().toString();
                String image = dataSnapshot.child("image").getValue().toString();
//                String thumb_image = dataSnapshot.child("thumb_image").getValue().toString();
                Picasso.with(SettingsActivity.this).load(image).into(mImageView);
                displayName.setText(name);
                settingsStatus.setText(status);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    void trocarStatus(View v){
        Intent statusPage = new Intent(this, StatusActivity.class);
        startActivity(statusPage);
    }

    void selectImage(View v){
//        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
//        galleryIntent.setType("image/*");
//        startActivityForResult(Intent.createChooser(galleryIntent, "Selecione uma Imagem"),PICK_IMAGE);
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1,1)
                .start(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                StorageReference filepath = mStorage.child("profile_images").child(currentUser.getUid()+".jpg");

                filepath.putFile(resultUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                        if (task.isSuccessful()){
                            @SuppressWarnings("VisibleForTests") String downloadUrl = task.getResult().getDownloadUrl().toString();
                            mDatabaseUser.child("image").setValue(downloadUrl);
                            Toast.makeText(SettingsActivity.this, "Imagem alterada com sucesso.", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(SettingsActivity.this, "Erro ao alterar imagem", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

}
