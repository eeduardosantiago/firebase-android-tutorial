package com.example.cess.firebasetuturial;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;

    private EditText mName;
    private EditText mEmail;
    private EditText mPassword;
    private Button mRegisterBtn;
    private Toolbar mToolbar;
    private ProgressDialog registerProgress;

    //private DatabaseReference mdatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mAuth = FirebaseAuth.getInstance();
        mEmail = (EditText) findViewById(R.id.registerPageEmail_editText);
        mName = (EditText) findViewById(R.id.registerPageNome_editText);
        mPassword = (EditText) findViewById(R.id.registerPageSenha_editText);
        mRegisterBtn = (Button)findViewById(R.id.registerPageRegister_button);

        mToolbar = (Toolbar)findViewById(R.id.registerPage_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Criar Nova Conta");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        registerProgress = new ProgressDialog(this);

        mRegisterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = mName.getText().toString();
                String email = mEmail.getText().toString();
                String password = mPassword.getText().toString();

                if (!TextUtils.isEmpty(name) || !TextUtils.isEmpty(email) || !TextUtils.isEmpty(password)){
                    registerProgress.setTitle("Criando uma Nova Conta...");
                    registerProgress.setCanceledOnTouchOutside(false);
                    registerProgress.show();
                    registerUser(name, email, password);
                }else
                    Toast.makeText(RegisterActivity.this,"Todos os Campos são obrigatórios",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void registerUser(final String name, String email, String password) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        //Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());

                        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
                        String userId = currentUser.getUid();
                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        DatabaseReference userReference = database.getReference().child("users").child(userId);

                        HashMap<String, String> userMap = new HashMap<String, String>();
                        userMap.put("nome", name);
                        userMap.put("status", "Fala vacilão! To usando o chat da Gnoa!!!");
                        userMap.put("image","default");
                        userMap.put("thumb_image","default");

                        userReference.setValue(userMap);


                        if (!task.isSuccessful()) {
                            registerProgress.hide();
                            Toast.makeText(RegisterActivity.this, R.string.auth_failed,
                                    Toast.LENGTH_LONG).show();
                        }else {
                            registerProgress.dismiss();
                            Intent MainActivityIntent = new Intent(RegisterActivity.this,MainActivity.class);
                            startActivity(MainActivityIntent);
                            finish();
                        }

                    }
                });
    }


}
