package com.example.cess.firebasetuturial;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class StatusActivity extends AppCompatActivity {

    Toolbar mToolbar;
    TextView newStatusTextView;
    DatabaseReference userReference;
    FirebaseUser currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);

        mToolbar = (Toolbar)findViewById(R.id.statusPageToolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Mude seu Status");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        newStatusTextView = (TextView)findViewById(R.id.statusPage_newStatus);


        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        userReference = FirebaseDatabase.getInstance().getReference().child("users").child(currentUser.getUid());

    }

    void mudarStatus(View v){
        String newStatus = newStatusTextView.getText().toString();
        userReference.child("status").setValue(newStatus).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful())
                    finish();
                else
                    Toast.makeText(StatusActivity.this, "Ops! Ocorreu um erro.", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
